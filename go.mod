module gitlab.com/gomimir/processor-imagick

go 1.16

require (
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	gitlab.com/gomimir/processor v0.28.0
	gopkg.in/gographics/imagick.v2 v2.6.0
)

// replace gitlab.com/gomimir/processor => ../processor
