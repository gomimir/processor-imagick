package thumbnailer

import (
	"fmt"
	"time"

	"github.com/rs/zerolog/log"
	mimir "gitlab.com/gomimir/processor"
	"gopkg.in/gographics/imagick.v2/imagick"
)

type Request struct {
	Width  *int
	Height *int
	Format mimir.ThumbnailFileFormat
}

type Thumbnail struct {
	mimir.ThumbnailDescriptor
	Bytes []byte
}

func GetThumbnailImagick(filename string, sourceBytes []byte, req Request) (*Thumbnail, error) {
	start := time.Now()

	mw := imagick.NewMagickWand()
	defer mw.Destroy()

	// We set filename to furthe help imagick determine the correct coders
	mw.SetFilename(filename)

	// Read the file
	err := mw.ReadImageBlob(sourceBytes)
	if err != nil {
		log.Error().Err(err).Msg("Unable to load image")
		return nil, err
	}

	// Go to page one, if it's an PDF file.
	mw.SetIteratorIndex(0)

	err = mw.AutoOrientImage()
	if err != nil {
		log.Error().Err(err).Msg("Unable to auto rotate the image")
		return nil, err
	}

	// Get original size
	width := mw.GetImageWidth()
	height := mw.GetImageHeight()

	log.Debug().Uint("width", width).Uint("height", height).Msg("Image loaded")

	var scaleRatio float64 = 1

	if req.Width != nil {
		scaleRatio = float64(*req.Width) / float64(width)
	}

	if req.Height != nil {
		hScaleRatio := float64(*req.Height) / float64(height)
		if hScaleRatio < scaleRatio {
			scaleRatio = hScaleRatio
		}
	}

	result := &Thumbnail{
		ThumbnailDescriptor: mimir.ThumbnailDescriptor{
			Format: req.Format,
			Width:  int(width),
			Height: int(height),
		},
	}

	if scaleRatio < 1 {
		result.Width = int(float64(width) * scaleRatio)
		result.Height = int(float64(height) * scaleRatio)

		d := log.Debug().Int("width", result.Width).Int("height", result.Height).Float64("ratio", scaleRatio)
		if req.Width != nil && req.Height != nil {
			d.Msg("Scaling down to fit")
		} else if req.Width != nil {
			d.Msg("Scaling down to width")
		} else {
			d.Msg("Scaling down to height")
		}

		err = mw.ThumbnailImage(uint(result.Width), uint(result.Height))
		if err != nil {
			log.Error().Err(err).Msg("Error occurred while trying to scale down the image")
			return nil, err
		}
	}

	if !req.Format.IsValid() {
		log.Error().Msgf("Invalid output file format %v", req.Format.DisplayName())
		return nil, fmt.Errorf("invalid output file format %v", req.Format.DisplayName())
	}

	err = mw.SetFormat(string(req.Format))
	if err != nil {
		log.Error().Err(err).Msgf("Unable to set output format to %v", req.Format.DisplayName())
		return nil, err
	}

	err = mw.SetImageAlphaChannel(imagick.ALPHA_CHANNEL_REMOVE)
	if err != nil {
		log.Error().Err(err).Msg("Unable to remove alpha channel")
		return nil, err
	}

	pw := imagick.NewPixelWand()
	defer pw.Destroy()

	pw.SetColor("rgb(255,255,255)")
	err = mw.SetImageBackgroundColor(pw)
	if err != nil {
		log.Error().Err(err).Msg("Unable to set background color")
		return nil, err
	}

	err = mw.SetImageCompressionQuality(95)
	if err != nil {
		log.Error().Err(err).Msg("Unable to set image compression")
		return nil, err
	}

	mw.ResetIterator()

	result.Bytes = mw.GetImageBlob()
	timeSpent := time.Since(start)
	log.Debug().Str("took", fmt.Sprintf("%v", timeSpent)).Int("bytes", len(result.Bytes)).Str("format", string(req.Format)).Msg("Thumbnail generated")

	return result, nil
}
