ARG DEV_IMAGE_TAG=master
ARG BASE_IMAGE_TAG=master

FROM registry.gitlab.com/gomimir/processor-imagick/dev:$DEV_IMAGE_TAG as build

# Download modules first to leverage layer caching
ADD go.mod /app/
ADD go.sum /app/
WORKDIR /app
RUN go mod download

# Build the app
ADD . /app
RUN make build

FROM registry.gitlab.com/gomimir/processor-imagick/base:$BASE_IMAGE_TAG
COPY --from=build /app/bin/mimir-processor-imagick /mimir-processor-imagick
CMD ["/mimir-processor-imagick", "process"]