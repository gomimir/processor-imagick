# Mimir thumbnailer

This app is a Mimir processor that generates cover images for indexed files.

For more information about how to use it see [documentation](https://gomimir.gitlab.io/processors/thumbnailer).

## Development

The processor is using ImageMagick v6 under the hood. Make sure you have it installed on your system.

If you don't want to be bothered by installing it you can do your development inside of docker container.

### Development inside Docker

Enter the development container

```bash
docker run --rm -ti -v `pwd`:/app -w /app registry.gitlab.com/gomimir/processor-imagick/dev:master bash
```

Develop inside as usual:

```bash
go run cmd/processor-imagick/processor.go test generateThumbnails testdata/your_test_image.jpg -o out.jpg
```

### Development without Docker on MacOS

Install Imagick using Brew:

```bash
brew install imagemagick@6
```

Develop (note: you need to set mentioned variables before you start your development, alternatively you can include them in your shell config):

```bash
export PKG_CONFIG_PATH="/usr/local/opt/imagemagick@6/lib/pkgconfig"
export CGO_CFLAGS_ALLOW='-Xpreprocessor'

go run cmd/processor-imagick/processor.go test generateThumbnails testdata/your_test_image.jpg -o out.jpg
```
